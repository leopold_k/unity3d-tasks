﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowCatapultBatton : MonoBehaviour
{
    [SerializeField] private GameObject _catapultCanvas;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        _catapultCanvas.SetActive(true);
    }

}
