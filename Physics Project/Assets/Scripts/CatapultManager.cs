﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CatapultManager : MonoBehaviour
{
    [SerializeField] private Animator _catapultAnimator;
    [SerializeField] private Button _CatapultButton;
    
    // Start is called before the first frame update
    void Start()
    {
        _CatapultButton.onClick.AddListener(StartCatapultButton);
    }

    void OnDisable ()
    {
        _CatapultButton.onClick.RemoveListener(StartCatapultButton);
    }
    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartCatapultButton()
    {
        _catapultAnimator.SetTrigger("StartCatapult");
    
    }
}
