﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class StartBall : MonoBehaviour
{
    [SerializeField] private LayerMask _layerMask;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            BallStart();
        }
    }

    private void BallStart()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, _layerMask))
        {
            if (hit.transform.gameObject.name == "Sphere" && hit.transform.gameObject.GetComponent<Rigidbody>() != null)
            {
                hit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = false;
            }
        }

    }
}
