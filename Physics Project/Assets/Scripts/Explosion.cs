﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    [SerializeField] private float _force;
    [SerializeField] private float _radius;
    [SerializeField] private GameObject _bomb;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.G))
        {
            ExplosionInner();
        }

    }

    private void ExplosionInner()
    {
        Debug.Log("Explosion");
        RaycastHit hit;
        Collider[] colliders = Physics.OverlapSphere(_bomb.transform.position, _radius);
        foreach (var item in colliders)
        {
            var rbCollider = item.GetComponent<Rigidbody>();
            if (rbCollider != null)
            {
                rbCollider.AddExplosionForce(_force, _bomb.transform.position, _radius);
            }
        }
    }


}
