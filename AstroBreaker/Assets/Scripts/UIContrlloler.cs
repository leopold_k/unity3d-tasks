﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIContrlloler : MonoBehaviour
{
    [SerializeField] 
    private WindowScript _setWindow;

    [SerializeField]
    private WindowScript _mainWindow;

    private void Awake()
    {
        _mainWindow.OpenButtonClickEvent += SetWindowOpenButtonClickEvent;
        _setWindow.CloseButtonClickEvent += SetWindowCloseButtonClickEvent;
    }

    private void SetWindowOpenButtonClickEvent()
    {
        _mainWindow.Hide();
        _setWindow.Show();
    }

    private void SetWindowCloseButtonClickEvent()
    {
        _mainWindow.Show();
        _setWindow.Hide();
    }

    void Start()
    {
        
    }

    void Update()
    {
        
    }
}
