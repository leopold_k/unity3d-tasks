﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AsteroidView : MonoBehaviour
{

    [SerializeField] public GameObject powerUpLongPrefub;
    [SerializeField] public GameObject powerUpSlowPrefub;
    public int MetallCountLives = 3;
    



    public void StartPowerUp()
    {

        var rnd = UnityEngine.Random.Range(0f, 100f);

        if (rnd < 30f)
        {
            GameObject PowerUpGO = Instantiate(powerUpLongPrefub, this.gameObject.transform.position, Quaternion.identity);
            PowerUpGO.SetActive(true);
            PowerUpGO.transform.localScale = new Vector3(0.3f, 0.3f, 1);

        }
        else if (rnd < 60f)
        {
            GameObject PowerUpGO = Instantiate(powerUpSlowPrefub, this.gameObject.transform.position, Quaternion.identity);
            PowerUpGO.SetActive(true);
            PowerUpGO.transform.localScale = new Vector3(0.3f, 0.3f, 1);
        }
        else
            return;
    }

    public void BlockDestroy()
    {
        
        gameObject.SetActive(false);
        
    }

    
}
