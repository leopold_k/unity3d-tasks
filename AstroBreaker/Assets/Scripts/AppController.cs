﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AppController : MonoBehaviour
{
    [SerializeField] private PlatformStartData _platformStartData;
    [SerializeField] private PlatformView _platformViewPrefab;
    [SerializeField] private KeybordDirectionDirector _keybordDirectionDirector;
    [SerializeField] private Ball_Move _ballPrefab;
    [SerializeField] private AsteroidView _asteroid1;
    [SerializeField] private AsteroidView _asteroid2;
    [SerializeField] private GameObject _gameOverPanel;
    [SerializeField] private Text ScoreText;
    [SerializeField] private AudioSource _ballSoundFX;
    private int MetallScore = 10;
    private int StoneScore = 5;
    private float BlockGenetationPercent = 30f;

    private int AsteroidCount = 15;
    public int ScoreCount;

   
    private PlatformView _platformView;
    private PlatformModel _platformModel;
    private PlatformController _platformController;
    [SerializeField] private GameController _gameController;




    void Start()
    {
        CreatePlatformModel();
        CreatePlatformView();
        CreatePlatformController();
        CreateBall();
        PalceAsteroid();
        CreateGameController();
        GetGameData();


    }


    void Update()
    {

    }

    private void GetGameData()
    {
        ScoreCount = FindObjectOfType<GameConditionHub>().score;
        FindObjectOfType<GameController>().lives = FindObjectOfType<GameConditionHub>().lives;
    }

    private void CreatePlatformView()
    {
        
        _platformView = Instantiate(_platformViewPrefab);
        _platformView.Init(_platformModel);
        FindObjectOfType<GameController>().OnTimerChangeEventSubscibe(_platformView);
        
    }

    private void CreateBall()
    {
        Ball_Move BallGO = Instantiate(_ballPrefab);
        FindObjectOfType<GameController>().CollisionEnterEventSubscibe(BallGO);
        FindObjectOfType<GameController>().OnLostBallSubscibe(BallGO);
        FindObjectOfType<GameController>().OnRaiseBallSizeTimerChangeEventSubscibe(BallGO);
        FindObjectOfType<Ball_Move>().OnBallSlowEventSubscibe(_platformView);
        


    }

    private void CreateGameController()
    {
        FindObjectOfType<GameController>().GameOverEvent += LoadGameOverPanel;
        FindObjectOfType<GameController>().NewBallEvent += CreateBall;
        FindObjectOfType<GameController>().BlockDestroyEvent += GameProcess;
    }

    

    private void GameProcess(GameObject gameObject)
    {
        if (gameObject.tag == "Metall")
        {
            ScoreCount = ScoreCount + MetallScore;
            AsteroidCount--;
        }
        else
        {
            ScoreCount = ScoreCount + StoneScore;
            AsteroidCount--;
        }
        //Debug.Log("Score: " + ScoreCount);
        ScoreText.text = "Score: " + ScoreCount;
        if (AsteroidCount == 0)
        {
            FindObjectOfType<GameConditionHub>().score = ScoreCount;
            FindObjectOfType<GameConditionHub>().lives = FindObjectOfType<GameController>().lives;
            FindObjectOfType<PlatformView>().gameObject.SetActive(false);
            FindObjectOfType<Ball_Move>().gameObject.SetActive(false);
            SceneController.I.LoadLevel(1);
        }
    }

    
    
    
    private void LoadGameOverPanel()
    {
        FindObjectOfType<PlatformView>().gameObject.SetActive(false);
        SceneController.I.LoadPrefabEndGame();
        //_gameOverPanel.gameObject.SetActive(true);
    }

    private void CreatePlatformModel()
    {
        _platformModel = new PlatformModel(_platformStartData.Speed);
    }

    private void CreatePlatformController()
    {
        _platformController = new PlatformController(_platformModel, _keybordDirectionDirector, _platformView);
    }

    private void PalceAsteroid()
    {
        
        
        for (int i = 0; i <= AsteroidCount; i++)
        {
            switch (i)
            {
                case 0:
                    //Instantiate(_asteroid1).transform.position = new Vector3(-1.9f, 2.69f, 0);
                    
                    if (UnityEngine.Random.Range(0f, 100f) < BlockGenetationPercent)
                    {
                        Instantiate(_asteroid1).transform.position = new Vector3(-1.9f, 2.69f, 0);
                    }
                    else Instantiate(_asteroid2).transform.position = new Vector3(-1.9f, 2.69f, 0);
                    break;
                case 1:
                    //Instantiate(_asteroid1).transform.position = new Vector3(-0.95f, 2.69f, 0);
                    if (UnityEngine.Random.Range(0f, 100f) < BlockGenetationPercent)
                    {
                        Instantiate(_asteroid1).transform.position = new Vector3(-0.95f, 2.69f, 0);
                    }
                    else Instantiate(_asteroid2).transform.position = new Vector3(-0.95f, 2.69f, 0);
                    break;
                case 2:
                    if (UnityEngine.Random.Range(0f, 100f) < BlockGenetationPercent)
                    {
                        Instantiate(_asteroid1).transform.position = new Vector3(-0.01f, 2.69f, 0);
                    }
                    else Instantiate(_asteroid2).transform.position = new Vector3(-0.01f, 2.69f, 0);
                    break;
                case 3:
                    if (UnityEngine.Random.Range(0f, 100f) < BlockGenetationPercent)
                    {
                        Instantiate(_asteroid1).transform.position = new Vector3(0.97f, 2.69f, 0);
                    }
                    else Instantiate(_asteroid2).transform.position = new Vector3(0.97f, 2.69f, 0);
                    break;
                case 4:
                    if (UnityEngine.Random.Range(0f, 100f) < BlockGenetationPercent)
                    {
                        Instantiate(_asteroid1).transform.position = new Vector3(1.9f, 2.69f, 0);
                    }
                    else Instantiate(_asteroid2).transform.position = new Vector3(1.9f, 2.69f, 0);
                    break;
                case 5:
                    if (UnityEngine.Random.Range(0f, 100f) < BlockGenetationPercent)
                    {
                        Instantiate(_asteroid1).transform.position = new Vector3(-1.9f, 2.26f, 0);
                    }
                    else Instantiate(_asteroid2).transform.position = new Vector3(-1.9f, 2.26f, 0);
                    break;
                case 6:
                    if (UnityEngine.Random.Range(0f, 100f) < BlockGenetationPercent)
                    {
                        Instantiate(_asteroid1).transform.position = new Vector3(-0.93f, 2.26f, 0);
                    }
                    else Instantiate(_asteroid2).transform.position = new Vector3(-0.93f, 2.26f, 0);
                    break;
                case 7:
                    if (UnityEngine.Random.Range(0f, 100f) < BlockGenetationPercent)
                    {
                        Instantiate(_asteroid1).transform.position = new Vector3(0.01f, 2.26f, 0);
                    }
                    else Instantiate(_asteroid2).transform.position = new Vector3(0.01f, 2.26f, 0);
                    break;
                case 8:
                    if (UnityEngine.Random.Range(0f, 100f) < BlockGenetationPercent)
                    {
                        Instantiate(_asteroid1).transform.position = new Vector3(0.96f, 2.26f, 0);
                    }
                    else Instantiate(_asteroid2).transform.position = new Vector3(0.96f, 2.26f, 0);
                    break;
                case 9:
                    if (UnityEngine.Random.Range(0f, 100f) < BlockGenetationPercent)
                    {
                        Instantiate(_asteroid1).transform.position = new Vector3(1.9f, 2.26f, 0);
                    }
                    else Instantiate(_asteroid2).transform.position = new Vector3(1.9f, 2.26f, 0);
                    break;
                case 10:
                    if (UnityEngine.Random.Range(0f, 100f) < BlockGenetationPercent)
                    {
                        Instantiate(_asteroid1).transform.position = new Vector3(-1.9f, 1.86f, 0);
                    }
                    else Instantiate(_asteroid2).transform.position = new Vector3(-1.9f, 1.86f, 0);
                    break;
                case 11:
                    if (UnityEngine.Random.Range(0f, 100f) < BlockGenetationPercent)
                    {
                        Instantiate(_asteroid1).transform.position = new Vector3(-0.95f, 1.86f, 0);
                    }
                    else Instantiate(_asteroid2).transform.position = new Vector3(-0.95f, 1.86f, 0);
                    break;
                case 12:
                    if (UnityEngine.Random.Range(0f, 100f) < BlockGenetationPercent)
                    {
                        Instantiate(_asteroid1).transform.position = new Vector3( 0, 1.86f, 0);
                    }
                    else Instantiate(_asteroid2).transform.position = new Vector3( 0, 1.86f, 0);
                    break;
                case 13:
                    if (UnityEngine.Random.Range(0f, 100f) < BlockGenetationPercent)
                    {
                        Instantiate(_asteroid1).transform.position = new Vector3(0.96f, 1.86f, 0);
                    }
                    else Instantiate(_asteroid2).transform.position = new Vector3(0.96f, 1.86f, 0);
                    break;
                case 14:
                    if (UnityEngine.Random.Range(0f, 100f) < BlockGenetationPercent)
                    {
                        Instantiate(_asteroid1).transform.position = new Vector3(1.9f, 1.86f, 0);
                    }
                    else Instantiate(_asteroid2).transform.position = new Vector3(1.9f, 1.86f, 0);
                    break;



            }





        }
        
        

    }

}


[Serializable]
public class PlatformStartData
{
    public float Speed;
}
