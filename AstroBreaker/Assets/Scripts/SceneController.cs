﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneController : MonoBehaviour
{
    public static SceneController I { get; private set; }

    [SerializeField] private GameObject _mainMenuPrefab;
    [SerializeField] private GameObject _endGamePanel;
    [SerializeField] private GameObject _scenesDocs;
    [SerializeField] private List<GameObject> _LevelList;
    


    public void LoadPrefab()
    {
        var sceneCount = _scenesDocs.transform.childCount;
       
        for (int i = sceneCount-1; i >= 0; i--)
        {
            Destroy(_scenesDocs.transform.GetChild(i).gameObject);
        }

        Instantiate(_mainMenuPrefab, _scenesDocs.transform);
    }

    public void LoadLevel(int id)
    {
        var sceneCount = _scenesDocs.transform.childCount;

        for (int i = sceneCount - 1; i >= 0; i--)
        {
            Destroy(_scenesDocs.transform.GetChild(i).gameObject);
        }

        Instantiate(_LevelList[id], _scenesDocs.transform);
    }

    public void LoadPrefabEndGame()
    {
        var sceneCount = _scenesDocs.transform.childCount;

        for (int i = sceneCount - 1; i >= 0; i--)
        {
            Destroy(_scenesDocs.transform.GetChild(i).gameObject);
        }

        //Instantiate(_mainMenuPrefab, _scenesDocs.transform);
        Instantiate(_endGamePanel, _scenesDocs.transform);
    }



    private void Awake()
    {
        I = this;
        LoadPrefab();
    }
}
