﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;



public class Ball_Move : MonoBehaviour
{
    Rigidbody2D rig;
    public float power_ball;
    private bool _isBallSlow = false;
    private float _timer;
    private const float _timeLarge = 5;
    public event Action<float> OnBallSizeTimerChangeEvent;
    public AudioSource ballSound;
    private float BallDeadLine = -6f;



    public event Action<GameObject> OnCollisionEnterEvent;

    public event Action OnLostBall;


    void Start()
    {
        rig = GetComponent<Rigidbody2D>();
        rig.AddForce(new Vector3(0.5f, 1, 2f) * Time.deltaTime * power_ball);
    }


    public void OnBallSlowEventSubscibe(PlatformView platformView)
    {
        
        platformView.BallSlowEvent += OnBallSlowEventHendler;
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        ballSound.Play();
        RaiseOnCollisionEnterEvent(collision.gameObject);
    }


    private void RaiseOnCollisionEnterEvent(GameObject gameObject)
    {
        OnCollisionEnterEvent?.Invoke(gameObject);

    }

    void Update()
    {
        
        if (transform.localPosition.y < BallDeadLine)
        {
            RaiseOnOnLostBall();
        }

        if (_isBallSlow)
        {
            if (_timer <= 0)
            {
                _timer = _timeLarge;
            }
            else
            {
                _timer -= Time.deltaTime;
                RaiseBallSizeTimerChangeEvent(_timer);
                if (_timer <= 0)
                {
                    FastBall();

                }
            }
        }

    }

    


    private void OnBallSlowEventHendler()
    {
        
        if (_isBallSlow)
        {
            _timer = _timeLarge;
            return;
        }

        SlowBall();

        
    }

    private void RaiseOnOnLostBall()
    {

        OnLostBall?.Invoke();
        gameObject.SetActive(false);
        
    }

    private void SlowBall()
    {

        rig = GetComponent<Rigidbody2D>();
        rig.AddForce(-Vector3.forward * Time.deltaTime * 2);
        _isBallSlow = true;
    }

    private void FastBall()
    {
        rig = GetComponent<Rigidbody2D>();
        rig.AddForce(Vector3.forward * Time.deltaTime * power_ball);//(new Vector3(0.5f, 1, 2f) * Time.deltaTime * power_ball); //;//(new Vector3(0.5f, 1, 2f) * Time.deltaTime * power_ball);
        _isBallSlow = false;
    }

    private void RaiseBallSizeTimerChangeEvent(float time)
    {
        OnBallSizeTimerChangeEvent?.Invoke(time);
    }
}
