﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

//public interface IBall
//{
//    event Action<Vector3> OnDirectionDetectedEvent;
//}

[Serializable]
public class GameController : MonoBehaviour
{

  
    private Ball_Move _ballPrefab;
    [SerializeField] private Text LargePlatformTimerText;
    [SerializeField] private Text SlowBallTimerText;
    
    private List<GameObject> _livesList;

    [SerializeField] public int lives;
    [SerializeField] private GameObject _livesDock;
    [SerializeField] private GameObject _oneLivePrefab;
    

    public event Action NewBallEvent;
    public event Action GameOverEvent;
    public event Action<GameObject> BlockDestroyEvent;

    public void Awake()
    {
         GetLives();
        
        _livesList = new List<GameObject>();

        for (int i = 0; i < lives; i++)
        {
            GameObject newLive = Instantiate(_oneLivePrefab, _livesDock.transform);
            _livesList.Add(newLive);
        }   
    }

    

    private void UpdateLivesPanel()
    {
        foreach (var item in _livesList)
        {
            item.SetActive(false);
        }
        for (int i = 0; i < lives; i++)
        {
            _livesList[i].SetActive(true);

        }
    
    }

    private void GetLives()
    {
        lives = FindObjectOfType<GameConditionHub>().lives;
    }


    public void CollisionEnterEventSubscibe(Ball_Move ballPrefab)
    {
        _ballPrefab = ballPrefab;
        _ballPrefab.OnCollisionEnterEvent += CollisionEnterEventHandler;
    }
    public void OnTimerChangeEventSubscibe(PlatformView platformView)
    {

        platformView.OnTimerChangeEvent += OnTimerChangeEventSubscibeHandler;
    }

    public void OnRaiseBallSizeTimerChangeEventSubscibe(Ball_Move ballPrefab)
    {
        ballPrefab.OnBallSizeTimerChangeEvent += OnRaiseBallSizeTimerChangeEventHandler;
    }


    private void CollisionEnterEventHandler(GameObject gameObject)
    {
        
        if (gameObject.tag != "Metall" && gameObject.tag != "Stone")
            return;
        gameObject.GetComponent<AsteroidView>().StartPowerUp();

        if (gameObject.tag == "Metall" && gameObject.GetComponent<AsteroidView>().MetallCountLives > 0)
        {
            gameObject.GetComponent<AsteroidView>().MetallCountLives--;
            //Debug.Log("MetallCountLives :" + gameObject.GetComponent<AsteroidView>().MetallCountLives);
            return;
        }
        
        gameObject.GetComponent<AsteroidView>().BlockDestroy();
        OnBlockDestroyEvent(gameObject);

    }

    private void OnBlockDestroyEvent(GameObject gameObject)
    {
        BlockDestroyEvent?.Invoke(gameObject);
    }

    public void OnLostBallSubscibe(Ball_Move ballPrefab)
    {
        _ballPrefab.OnLostBall += OnLostBallHandler;
    }

    public void OnLostBallHandler()
    {
        if (lives != 0)
        {
            lives--;
            UpdateLivesPanel();
            OnNewBallEvent();
        }
        else
            OnGameOverEvent();

    }

    private void OnNewBallEvent()
    {
        NewBallEvent?.Invoke();
    }

    private void OnGameOverEvent()
    {
        GameOverEvent?.Invoke();
    }

    private void OnTimerChangeEventSubscibeHandler(float time)
    {
        LargePlatformTimerText.text = "Time" + time.ToString("F1");
    }

    private void OnRaiseBallSizeTimerChangeEventHandler(float time)
    {
        SlowBallTimerText.text = "Time" + time.ToString("F1");

    }

    private void OnDestroy()
    {
        _ballPrefab.OnCollisionEnterEvent -= CollisionEnterEventHandler;
    }

}
