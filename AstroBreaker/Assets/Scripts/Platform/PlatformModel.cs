﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformModel : MonoBehaviour
{
    private float _speed;
    private float _defaultSpeed;
    public float RealSpeed { get { return _speed; } }

    public PlatformModel(float platformSpeed)
    {
        _defaultSpeed = platformSpeed;
        ResetSpeed();
    }

    public void SetSpeed(float speed)
    {
        _speed = speed;
    }

    public void ResetSpeed()
    {
        _speed = _defaultSpeed;
    }
}
