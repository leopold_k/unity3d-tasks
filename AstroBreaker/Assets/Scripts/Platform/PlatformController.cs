﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlatformDirection
{
	Vector3 Direction { get; }
	event Action<Vector3> OnDirectionDetectedEvent;
}


public class PlatformController 
{
	private readonly PlatformModel _platformModel;
	private readonly IPlatformDirection _platformDirection;
	private readonly PlatformView _platformView;
	private readonly PlatformStartData _platformStartData;
	

	public PlatformController (PlatformModel platformModel, IPlatformDirection platformDirection, PlatformView platformView)
    {
		_platformModel = platformModel;

		_platformDirection = platformDirection;
		_platformDirection.OnDirectionDetectedEvent += OnDirectionDetectedHandler;

		_platformView = platformView;

	}

	


	private void OnDirectionDetectedHandler(Vector3 vector3_GO)
	{
		
		_platformView.PlatformTransform.position += vector3_GO * _platformModel.RealSpeed;
	}
}
