﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeybordDirectionDirector : MonoBehaviour, IPlatformDirection
{
    public Vector3 Direction
    {
        get
        {
            return _direction;
            
        }
    
    }

    public event Action<Vector3> OnDirectionDetectedEvent;

    private Vector3 _direction = Vector3.zero;

    void Start()
    {
        
    }

   
    void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            RiseDirectionDetectedEvent(Vector3.left);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            RiseDirectionDetectedEvent(Vector3.right);
        }

        _direction = Vector3.zero;
    }

    private void RiseDirectionDetectedEvent(Vector3 direction)
    {
        _direction = direction;
        OnDirectionDetectedEvent?.Invoke(direction);
    }
}
