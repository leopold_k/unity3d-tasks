﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformView : MonoBehaviour
{
    private PlatformModel _platformModel;
    private bool _isScaledPlatform;
    private float _timer;
    private const float _timeLarge = 5;
    public event Action<float> OnTimerChangeEvent;
    public event Action BallSlowEvent;


    public event Action<GameObject> OnTriggerEnterEvent;
    public Transform PlatformTransform => transform;

    public void Init(PlatformModel platformModel)
    {
        _platformModel = platformModel;
    }
    
    void Start()
    {

    }

    void Update()
    {
        if (_isScaledPlatform)
        {
            if (_timer <= 0)
            {
                _timer = _timeLarge;
            }
            else
            {
                _timer -= Time.deltaTime;
                RaiseOnTimerChangeEvent(_timer);
                if (_timer <= 0)
                {
                    SmallPlatform();

                }
            }
        }
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {

        ScalePlatformAvction(collision.gameObject);
        if (collision.gameObject.tag == "SlowBall")
        {
            
            RaiseOnBallSlowEvent();
            collision.gameObject.SetActive(false);
        }
    }

    private void ScalePlatformAvction(GameObject gameObject)
    {
        if (gameObject.tag != "LargeScale")
            return;
        if (_isScaledPlatform)
        {
            _timer = _timeLarge;
            return;
        }

        LargePlatform();

        gameObject.SetActive(false);
    }

    private void BallMove(GameObject gameObject)
    {

    }


    private void RaiseOnTriggerEnterEvent(GameObject gameObject)
    {
        OnTriggerEnterEvent?.Invoke(gameObject);
    }

    private void RaiseOnTimerChangeEvent(float time)
    {
        OnTimerChangeEvent?.Invoke(time);
    }

    private void RaiseOnBallSlowEvent()
    {
        BallSlowEvent?.Invoke();
    }

    private void LargePlatform()
    {
        gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x*2, gameObject.transform.localScale.y, gameObject.transform.localScale.z);
        _isScaledPlatform = true;
    }

    private void SmallPlatform()
    {
        gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x / 2, gameObject.transform.localScale.y, gameObject.transform.localScale.z); ;
        _isScaledPlatform = false;
    }


}
