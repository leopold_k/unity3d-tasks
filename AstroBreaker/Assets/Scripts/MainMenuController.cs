﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuController : MonoBehaviour
{
    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void LoadNewLevel(int id)
    {
        SceneController.I.LoadLevel(id-1);
    }

    public void LoadMainMenu()
    {
        SceneController.I.LoadPrefab();
    }

    


}
