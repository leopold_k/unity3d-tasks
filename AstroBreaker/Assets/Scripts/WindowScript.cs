﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WindowScript : MonoBehaviour
{

    [SerializeField]
    private Button _setButton;

    [SerializeField]
    private Button _rtrnButton;

    public event Action CloseButtonClickEvent;
    public event Action OpenButtonClickEvent;

    private void Awake()
    {
        _setButton.onClick.AddListener(OpenButtonClick);
        _rtrnButton.onClick.AddListener(CloseButtonClick);
    }

    private void OpenButtonClick()
    {
        OpenButtonClickEvent?.Invoke();
    }

    private void CloseButtonClick()
    {
        CloseButtonClickEvent?.Invoke();
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }


    void Start()
    {
        
    }

    
    void Update()
    {
        
    }
}
